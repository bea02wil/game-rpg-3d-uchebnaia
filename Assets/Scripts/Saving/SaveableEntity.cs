﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace RPG.Saving
{
    [ExecuteAlways]
    public class SaveableEntity : MonoBehaviour
    {
        [SerializeField] string uniqueIdentifier = "";
        static Dictionary<string, SaveableEntity> globalLookup = new Dictionary<string, SaveableEntity>();

        public string GetUniqueIdentifier()
        {
            return uniqueIdentifier;
        }

        public object CaptureState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureState();
            }
            return state;
            
        }

        public void RestoreState(object state)
        {
            Dictionary<string, object> stateDict = (Dictionary<string, object>)state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreState(stateDict[typeString]);
                }
            }         
        }

#if UNITY_EDITOR //чтобы при сборке проекта, код внутри не компилировался. Если в эдиторе, то выполнять код, если не в эдиторе, не захватывать
        private void Update()
        {
            if (Application.IsPlaying(gameObject)) return;//вернет тру, если аргумент при запуске игры есть в сцене, то есть запущен
            if (string.IsNullOrEmpty(gameObject.scene.path)) return; //если путь к GO пуст или null, то мы находимся в префабе, а значит, надо выйти из метода
            //т.е путь к инстансу в сцене был бы Assets/Scenes/Sandbox, а пусть к префабу лежит в не в сцене, а сам по себе в Assets, поэтому возвращает null

            //To change the value serialized to a scene file or prefab we should...
            //use a SerializedObject to modify the value
            SerializedObject serializeObject = new SerializedObject(this);
            SerializedProperty property = serializeObject.FindProperty("uniqueIdentifier"); //таким образом обращаемся к полям объекта        

            //такой подход работать не будет, потому что
            // при изменении свойств юнити знать об этом не будет и генерироваться новое ничего не будет
          //  и замена этому SerializedObject
            //if (uniqueIdentifier == "")            
            //{
            //    uniqueIdentifier = System.Guid.NewGuid().ToString();
            //}

            if (string.IsNullOrEmpty(property.stringValue) || !IsUnique(property.stringValue))
            {
                property.stringValue = System.Guid.NewGuid().ToString();
                serializeObject.ApplyModifiedProperties();//говорим юнити, что произошло изменение полей
            }

            globalLookup[property.stringValue] = this;
            //Все это хранит информацию в сцене, те в Sandbox, при перезагрузки игры информация будет браться из нее
            print("Editing");
        }

        //Чтобы у дубликатов объектов, наприм, enemy, не было одинаковых guid
        private bool IsUnique(string candidate) 
        {
            if (!globalLookup.ContainsKey(candidate)) return true;

            if (globalLookup[candidate] == this) return true;

            //при переходена сл сцену в первой стираются id, потому что globalLookup словарь статичный, что подразумевает
            //жизнь словаря на протяжении работы приложения, а значит, сохранятеся и между сценами
            if (globalLookup[candidate] == null) 
            {
                globalLookup.Remove(candidate);
                return true;
            }

            if (globalLookup[candidate].GetUniqueIdentifier() != candidate)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            return false;
        }
#endif
    }
}
