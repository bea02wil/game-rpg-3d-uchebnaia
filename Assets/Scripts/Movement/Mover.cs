﻿using RPG.Core;
using UnityEngine;
using UnityEngine.AI;
using RPG.Saving;
using RPG.Attributes;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] Transform target;
        [SerializeField] float maxSpeed = 6f;
        [SerializeField] float maxNavPathLength = 40f;

        NavMeshAgent navMeshAgent;
        Health health;
        Animator animator;

        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            health = GetComponent<Health>();
            animator = GetComponent<Animator>();
        }       

        void Update()
        {
            navMeshAgent.enabled = !health.IsDead;

            UpdateAnimator();
            // Debug.DrawLine(lastRay.origin, lastRay.direction * 100); рисует линию от камеры в точку клика. 100 - это длина линии            
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);          
            MoveTo(destination, speedFraction);
        }

        public bool CanMoveTo(Vector3 destination)
        {
            NavMeshPath path = new NavMeshPath();
            bool hasPath = NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            if (!hasPath) return false;
            if (path.status != NavMeshPathStatus.PathComplete) return false;
            if (GetPathLength(path) > maxNavPathLength) return false;

            return true;
        }

        public void MoveTo(Vector3 destination, float speedFraction)
        {
            navMeshAgent.destination = destination;
            navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction); //ограничивает значение между 0 и 1 и возвращает его
            navMeshAgent.isStopped = false;
        }

        public void Cancel()//отменяет передвижение
        {
            navMeshAgent.isStopped = true;
        }   

        private void UpdateAnimator()
        {
            Vector3 velocity = navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            animator.SetFloat("forwardSpeed", speed);
        }

        private float GetPathLength(NavMeshPath path)
        {
            float total = 0;
            if (path.corners.Length < 2) return total;
            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                total += Vector3.Distance(path.corners[i], path.corners[i + 1]);
            }

            return total;
        }

        //  [System.Serializable] //структура неявно несериализизованная
        //  struct MoverSaveData
        //  {
        //      public SerializableVector3 position;
        //      public SerializableVector3 rotation;
        // }

        public object CaptureState()
        {
            //Способ со словарем: //словарь явно сериализован
            //Dictionary<string, object> data = new Dictionary<string, object>();
            //data["position"] = new SerializableVector3(transform.position);
            //data["rotation"] = new SerializableVector3(transform.eulerAngles);
            //return data;

            //Способ со структурой:
            /*MoverSaveData data = new MoverSaveData();
            data.position = new SerializableVector3(transform.position);
            data.rotation = new SerializableVector3(transform.eulerAngles);
            return data; */

            return new SerializableVector3(transform.position);
        }

        public void RestoreState(object state)
        {
            ////Способ со словарем:
            /* Dictionary<string, object> data = (Dictionary<string, object>)state;
               GetComponent<NavMeshAgent>().enabled = false; //чтобы не было проблем с NavMeshAgent
               transform.position = ((SerializableVector3)data["position"]).ToVector();
               transform.eulerAngles = ((SerializableVector3)data["rotation"]).ToVector();
               GetComponent<NavMeshAgent>().enabled = true; */
            //или так: GetComponent<NavMeshAgent>().Move();

            //Способ со структурой:
            /*MoverSaveData data = (MoverSaveData)state;
            GetComponent<NavMeshAgent>().enabled = false;
            transform.position = data.position.ToVector();
            transform.eulerAngles = data.rotation.ToVector();
            GetComponent<NavMeshAgent>().enabled = true; */
            //    SerializableVector3 position = (SerializableVector3)state;
            //    GetComponent<NavMeshAgent>().enabled = false;
            //    transform.position = position.ToVector();           
            //     GetComponent<NavMeshAgent>().enabled = true;
            SerializableVector3 position = (SerializableVector3)state;
            navMeshAgent.enabled = false;
            transform.position = position.ToVector();
            navMeshAgent.enabled = true;
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
    }
}
