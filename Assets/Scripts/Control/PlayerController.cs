﻿using UnityEngine;
using RPG.Movement;
using RPG.Attributes;
using UnityEngine.AI;
using System;
using UnityEngine.EventSystems;

namespace RPG.Control
{
    public class PlayerController : MonoBehaviour
    {
        Health health;
      
        [Serializable]
        struct CursorMapping
        {
            public CursorType type;
            public Texture2D texture;
            public Vector2 hotspot;
        }

        [SerializeField] CursorMapping[] cursorMapping = null;
        [SerializeField] float maxNavMeshProjectionDistance = 1f;
        [SerializeField] float raycastRadius = 0;

        private void Awake()
        {
            health = GetComponent<Health>();
        }

        private void Update()
        {
            if (InteractWithUI()) return;
            if (health.IsDead)
            {
                SetCursor(CursorType.None);
                return;
            }

            if (InteractWithComponent()) return;           
            if (InteractWithMovement()) return;

            SetCursor(CursorType.None);
        }
      
        private bool InteractWithUI()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                SetCursor(CursorType.UI);
                return true;
            }
            return false;
        }

        private bool InteractWithComponent()
        {
            RaycastHit[] hits = RaycastAllSorted();
            foreach (RaycastHit hit in hits)
            {
                IRaycastable[] raycastables = hit.transform.GetComponents<IRaycastable>();
                foreach (IRaycastable raycastable in raycastables)
                {
                    if (raycastable.HandleRaycast(this))
                    {
                        SetCursor(raycastable.GetCursorType());
                        return true;
                    }
                }
            }
            return false;
        }

        private RaycastHit[] RaycastAllSorted()
        {
            //Так было до агро рядом стоящих врагов
            //RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            //float[] distances = new float[hits.Length];
            //for (int i = 0; i < hits.Length; i++)
            //{
            //    distances[i] = hits[i].distance;
            //}
            ////Сортирует хиты по дистанции, от малой к большой 
            //Array.Sort(distances, hits);
            //return hits;

            RaycastHit[] hits = Physics.SphereCastAll(GetMouseRay(), raycastRadius);
            float[] distances = new float[hits.Length];
            for (int i = 0; i < hits.Length; i++)
            {
                distances[i] = hits[i].distance;
            }
            //Сортирует хиты по дистанции, от малой к большой 
            Array.Sort(distances, hits);
            return hits;
        }

        //Так было до момента перетаскивания этого кода в CombatTarger
        //private bool InteractWithCombat()
        //{
        //    RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
        //    foreach (RaycastHit hit in hits)
        //    {              
        //CombatTarget target = hit.transform.GetComponent<CombatTarget>();
        //if (target == null) continue;


        //if (!GetComponent<Fighter>().CanAttack(target.gameObject)) continue;

        //if (Input.GetMouseButton(0))
        //{
        //    GetComponent<Fighter>().Attack(target.gameObject);
        //}
        //SetCursor(CursorType.Combat);
        //return true;
        //}
        //return false;

        //Выше тоже самое что и код ниже - InteractWithCombat()
        //if (Input.GetMouseButtonDown(0))
        // {
        //     Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //     bool hasHit = Physics.Raycast(ray, out RaycastHit hit);
        //     if (hasHit)
        //     {
        //         CombatTarget target = hit.transform.GetComponent<CombatTarget>();

        //         if (target)
        //         {
        //             GetComponent<Fighter>().Attack(target);                         
        //         }               
        //    }
        // }           
        //}

        private bool InteractWithMovement()
        {
            //так было до:
            //Ray ray = GetMouseRay();
            //RaycastHit hit;
            //bool hasHit = Physics.Raycast(ray, out hit);
            //if (hasHit)
            //{
            //    if (Input.GetMouseButton(0))  //работает, даже если зажата кнопка
            //    {
            //        GetComponent<Mover>().StartMoveAction(hit.point, 1f);
            //    }
            //    SetCursor(CursorType.Movement);
            //    return true;
            //}
            //return false;

            Vector3 target;
            bool hasHit = RaycastNavMesh(out target);
            if (hasHit)
            {
                if (!GetComponent<Mover>().CanMoveTo(target)) return false;

                if (Input.GetMouseButton(0))
                {
                    GetComponent<Mover>().StartMoveAction(target, 1f);
                }
                SetCursor(CursorType.Movement);
                return true;
            }
            return false;
        }

        private bool RaycastNavMesh(out Vector3 target)
        {
            target = new Vector3();

            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
            if (!hasHit) return false;

            NavMeshHit navMeshHit;
            bool hasCastToNavMesh = NavMesh.SamplePosition(
                hit.point, out navMeshHit, maxNavMeshProjectionDistance, NavMesh.AllAreas);
            if (!hasCastToNavMesh) return false;


            target = navMeshHit.position;
          
            return true;
        }
      
        private void SetCursor(CursorType type)
        {
            CursorMapping mapping = GetCursorMapping(type);
            Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
        }

        private CursorMapping GetCursorMapping(CursorType type)
        {
            foreach (CursorMapping mapping in cursorMapping)
            {
                if (mapping.type == type)
                {
                    return mapping;
                }
            }
            return cursorMapping[0];
        }

        private static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}


