﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class CameraFacing : MonoBehaviour
    {           
        void LateUpdate()//чтобы Health Bar при вращении противника не дергался. LateUpdate потому что сама камера в LateUpdate запущена
        {
            transform.forward = Camera.main.transform.forward;
        }
    }
}
