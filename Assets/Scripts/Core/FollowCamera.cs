﻿using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target;


        void LateUpdate() //чтобы камера выполнялась после Update методов других скриптов
        {
            transform.position = target.position;
        }
    }
}
