﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace RPG.Stats
{
    [CreateAssetMenu(fileName = "Progression", menuName = "Stats/New Progression", order = 0)]
    public class Progression : ScriptableObject
    {
        [SerializeField] ProgressionCharacterClass[] characterClasses = null;

        Dictionary<CharacterClass, Dictionary<Stat, float[]>> lookupTable = null;

        public float GetStat(Stat stat, CharacterClass characterClass, int level)
        {
            BuildLoookup();

            float[] levels = lookupTable[characterClass][stat];

            if (levels.Length < level) return 0;

            return levels[level - 1];

            //Это был вариант ДО
            //foreach (ProgressionCharacterClass progressionClass in characterClasses)
            //{
            //    if (progressionClass.characterClass != characterClass) continue;
                
            //    foreach (ProgressionStat progressionStat in progressionClass.stats)
            //    {
            //        if (progressionStat.stat != stat) continue;

            //        if (progressionStat.levels.Length < level) continue;

            //        return progressionStat.levels[level - 1];
            //    }
            //}
            //return 0;
        }

        public int GetLevels(Stat stat, CharacterClass characterClass)
        {
            BuildLoookup();

            float[] levels = lookupTable[characterClass][stat];
            return levels.Length;
        }

        private void BuildLoookup()
        {
            if (lookupTable != null) return;

            lookupTable = new Dictionary<CharacterClass, Dictionary<Stat, float[]>>();

            foreach (ProgressionCharacterClass progressionClass in characterClasses)
            {
                Dictionary<Stat, float[]> statLookupTable = new Dictionary<Stat, float[]>();

                foreach (ProgressionStat progressionStat in progressionClass.stats)
                {
                    statLookupTable[progressionStat.stat] = progressionStat.levels;
                }

                lookupTable[progressionClass.characterClass] = statLookupTable;
            }
        }

        [System.Serializable]
        class ProgressionCharacterClass
        {
            //Если не сделать Public поля или [SerializeField] и сам класс [Serializable], то в инспекторе видно не будет
            //Придется делать  public, т.к. в методе GetHealth ругался бы на эти поля, потому что они приватные
            //public поле явл уже [Serializeable]
            public CharacterClass characterClass;
            public ProgressionStat[] stats;
           
        }

        [System.Serializable]
        class ProgressionStat
        {
            public Stat stat;
            public float[] levels;
        }
    }
}
