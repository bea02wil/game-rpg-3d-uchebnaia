﻿using UnityEngine;
using System.Collections;

namespace RPG.SceneManagement
{
    public class Fader : MonoBehaviour
    {
        CanvasGroup canvasGroup;
        Coroutine currentActiveFade = null;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
           // StartCoroutine(FadeOutIn());  -это был пример использования вложенных (nested) корутин
        }

        //IEnumerator FadeOutIn() - -это был пример использования вложенных (nested) корутин
        //{
        //    yield return FadeOut(3f);
        //    print("Faded out");
        //    yield return FadeIn(1f);
        //    print("Faded in");
        //}

        public Coroutine FadeOut(float time)
        {
            return Fade(1, time);
        }

        private IEnumerator FadeRoutine(float target, float time)
        {
            while (!Mathf.Approximately(canvasGroup.alpha, target))
            {
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, target, Time.deltaTime / time);
                yield return null; //подождать один фрейм
            }
        }

        public Coroutine FadeIn(float time)
        {
            return Fade(0, time);
        }

        public Coroutine Fade(float target, float time)
        {
            if (currentActiveFade != null)
            {
                StopCoroutine(currentActiveFade);
            }
            currentActiveFade = StartCoroutine(FadeRoutine(target, time));
            return currentActiveFade;
        }

        public void FadeOutImmediate()
        {
            canvasGroup.alpha = 1;
        }
    }
}