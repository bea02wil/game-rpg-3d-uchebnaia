﻿using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
    public class CinematicTrigger : MonoBehaviour
    {
        bool alreadyTriggered = true;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player" && alreadyTriggered)
            {
                alreadyTriggered = false;
                GetComponent<PlayableDirector>().Play();               
            }
            
        }
    }
}
